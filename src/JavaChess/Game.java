package JavaChess;

import JavaChess.Pieces.*;

import java.awt.*;
import java.awt.image.BufferStrategy;

import static JavaChess.MatedStatus.*;

public class Game implements Runnable {
    // WINDOW
    public String title;
    private Display display;
    public int width, height;


    private MouseManager mouseManager;


    // BOARD VAlUES
    public int SquareSize = 70;
    private int boardSize = SquareSize * 8;

    public int BoardXOffset = (boardSize/2) + (width/2) - 125;
    public int BoardYOffset = 5;

    public Tile[][] Board; // [y][x]

    // Colors
    public Color selectedColor = new Color(0x7367FF);
    public Color checkColor = new Color(0xFF0500);
    public Color validMoveColor = new Color(0x66FF58);
    public Color lightColor = new Color(0xFFC363);
    public Color darkColor = new Color(0x3A2A1B);

    // GAME STATE
    private Tile[] validMovesForSelected;
    private Tile selectedTile;
    private boolean turn = true; // TRUE = White, False = Black
    private MatedStatus gameState = NoOneMated;

    private King whiteKing;
    private King blackKing;

    // GRAPHICS
    private BufferStrategy bufferStrategy;
    private Graphics graphics;

    // THREAD
    private Thread thread;
    private boolean running;


    public Game(String title, int w, int h) {
        width = w;
        height = h;
        this.title = title;
        mouseManager = new MouseManager();

        Board = new Tile[8][8];

        generateBoard();

        System.out.println("Initialized Game");
    }

    private void generateBoard() {
        // GENERATE BOARD:
        for (int i = 0; i < Board.length; i++) {
            Tile[] row = Board[i];
            for (int j = 0; j < row.length; j++) {
                Tile tile = new Tile();

                tile.x = j;
                tile.y = i;

                if(i == 1 || i == 6) {
                    Colour colour = (i==1)? Colour.Black : Colour.White;
                    tile.piece = new Pawn(j,i,colour);
                } else if (i == 0 || i == 7) {
                    Colour colour = (i==0)? Colour.Black : Colour.White;

                    switch (j) {
                        case 0:
                        case 7:
                            tile.piece = new Rook(j,i,colour);
                            break;
                        case 1:
                        case 6:
                            tile.piece = new Knight(j,i, colour);
                            break;
                        case 2:
                        case 5:
                            tile.piece = new Bishop(j,i,colour);
                            break;
                        case 3:
                            tile.piece = new Queen(j,i, colour);
                            break;
                        case 4:
                            tile.piece = new King(j,i, colour);
                            break;


                    }
                }
                Board[i][j] = tile;
            }
        }
    }


    private Tile getTile(int x, int y) throws TileNotFound {
        float boardX, boardY;
        boardX = x - BoardXOffset;
        boardY = y - BoardYOffset;


        int tileX = (int) Math.floor(boardX/SquareSize);
        int tileY = (int) Math.floor(boardY/SquareSize);

        if(tileX > 7 || tileY > 7) {
            throw new TileNotFound();
        }
        return Board[tileY][tileX];
    }

    private void init() {
        display = new Display(title, width, height);

        display.getCanvas().addMouseListener(mouseManager);

        whiteKing = Piece.getKing(Board, Colour.White);
        blackKing = Piece.getKing(Board, Colour.Black);
    }

    private void update() {
        if(mouseManager.left) {
            System.out.println("Left clicked");
            try {
                Tile newSel = getTile(mouseManager.x, mouseManager.y);
                if(newSel == selectedTile) {
                    selectedTile = null;
                    validMovesForSelected = null;
                } else if(newSel.isIn(validMovesForSelected)){
                    // Make the move
                    selectedTile.piece.Move(Board, newSel);
                    whiteKing.inCheck = whiteKing.isInCheck(Board);
                    blackKing.inCheck = blackKing.isInCheck(Board);
                    gameState = Piece.checkForMate(Board);
                    turn = !turn;
                    selectedTile = null;
                    validMovesForSelected = null;
                } else {
                    selectedTile = newSel;
                    if(selectedTile.piece != null) {
                        if(turn && selectedTile.piece.colour == Colour.White) {
                            validMovesForSelected = selectedTile.piece.getValidMoves(Board);
                        } else if(!turn && selectedTile.piece.colour == Colour.Black) {
                            validMovesForSelected = selectedTile.piece.getValidMoves(Board);
                        } else {
                            validMovesForSelected = null;
                        }
                    } else {
                        validMovesForSelected = null;
                    }

                    System.out.printf("Clicked x: %d y: %d\n",selectedTile.x, selectedTile.y);
                }
            } catch (TileNotFound e) {
                e.printStackTrace();
            }
            mouseManager.left = false;
        } else if (mouseManager.right) {
            System.out.println("Right clicked");
            mouseManager.right = false;
        }
    }



    private void drawBoard() {
        for (int i = 0; i<8; i++) {

            for (int j = 0; j<8; j++) {

                // DETERMINE THE COLOR OF THE SQUARE:
                Color color;
                Tile t = Board[i][j]; // tile to be drawn

                switch ((j + i) % 2) {
                    case 0:
                        color = lightColor;
                        break;
                    case 1:
                        color = darkColor;
                        break;
                    default:
                        throw new IllegalStateException("Unexpected value: " + (j + i) % 2);
                }
                if(selectedTile != null) {
                    if(t.x == selectedTile.x && t.y == selectedTile.y) {
                        color = selectedColor;
                    }
                }

                if(validMovesForSelected != null) {
                    for (Tile tile: validMovesForSelected) {
                        if(tile.x == t.x && tile.y == t.y) {
                            color = validMoveColor;
                        }
                    }
                }

                if(t.piece == whiteKing && whiteKing.inCheck) {
                    color = checkColor;
                }
                if(t.piece == blackKing && blackKing.inCheck) {
                    color = checkColor;
                }

                graphics.setColor(color);
                graphics.fillRect(BoardXOffset + SquareSize * j, BoardYOffset + SquareSize * i, SquareSize, SquareSize);
            }
        }
        graphics.setColor(new Color(0));

        // DRAWING OF THE COORDINATES
        for (int i = 0; i < 8; i++) {
            graphics.drawString(String.valueOf(8-i), BoardXOffset + SquareSize * 8 + 5, BoardYOffset + SquareSize * i + 30);
        }

        for (int i = 0; i < 8; i++) {
            char character = (char) (97 + i);
            graphics.drawString(String.valueOf(character), BoardXOffset + SquareSize * i + 20, BoardYOffset + SquareSize * 8 + 15);
        }
    }

    private void drawPieces() {
        Font font = new Font("Arial", Font.PLAIN, 30);
        Color White = new Color(0xFFFFFF);
        Color Black = new Color(0);
        graphics.setFont(font);
        for (int i = 0; i < Board.length; i++) {
            Tile[] row = Board[i];
            for (int j = 0; j < row.length; j++) {
                Tile tile = Board[i][j];


                if(tile.piece == null) {
                    continue;
                }
                Color drawColor = (tile.piece.colour == Colour.White)? White : Black;

                graphics.setColor(drawColor);
                if(tile.piece.getImage() == null) {
                    graphics.drawString(tile.piece.toString(), BoardXOffset + SquareSize * j + 25, BoardYOffset + SquareSize * i + 50);
                } else {
                    graphics.drawImage(tile.piece.getImage(),BoardXOffset + SquareSize * j + 2, BoardYOffset + SquareSize * i +5, null);
                }

            }
        }
    }

    private void render() {
        // INIT
        bufferStrategy = display.getCanvas().getBufferStrategy();
        if(bufferStrategy == null) {
            display.getCanvas().createBufferStrategy(3);
            return;
        }
        graphics = bufferStrategy.getDrawGraphics();
        graphics.clearRect(0,0,width, height);
        // DRAWING
        graphics.setColor(new Color(0x656565));
        graphics.fillRect(0,0,width,height);

        drawBoard();
        drawPieces();


        Font font = new Font("Arial", Font.PLAIN, 20);
        graphics.setFont(font);
        if(turn) {
            graphics.setColor(Color.WHITE);
            graphics.drawString("Whites turn", width - 110 ,height - 10);
        } else {
            graphics.setColor(Color.BLACK);
            graphics.drawString("Blacks turn", width - 110 ,height - 10);
        }
        font = new Font("Arial", Font.BOLD, 60);
        graphics.setFont(font);

        switch (gameState) {
            case BlackMated:
                graphics.setColor(Color.WHITE);
                graphics.drawString("White wins", BoardXOffset + boardSize / 2 - 140,height / 2);
                break;
            case WhiteMated:
                graphics.setColor(Color.BLACK);
                graphics.drawString("Black wins", BoardXOffset + boardSize / 2 - 140,height / 2);
                break;
            case StaleMate:
                graphics.setColor(Color.LIGHT_GRAY);
                graphics.drawString("Draw", BoardXOffset + boardSize / 2 - 110,height / 2);
                break;
        }
        // CLEANUP
        bufferStrategy.show();
        graphics.dispose();

    }


    public void run() {
        init();

        while (running) {
            update();
            render();
        }

        stop();
    }

    public synchronized void start() {
        if(running)
            return;
        running = true;
        thread = new Thread(this);
        thread.start();
    }

    public synchronized void stop() {
        if(!running)
            return;
        running = false;
        try {
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
