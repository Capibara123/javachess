package JavaChess;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
public class MouseManager implements MouseListener {
    public boolean left, right;
    public int x, y;
    MouseManager() {
        left = false;
        right = false;
    }
    @Override
    public void mouseClicked(MouseEvent mouseEvent) {
        switch (mouseEvent.getButton()) {
            case MouseEvent.BUTTON1:
                left = true;
                break;
            case MouseEvent.BUTTON3:
                right = true;
                break;
        }
        x = mouseEvent.getX();
        y = mouseEvent.getY();
    }

    @Override
    public void mousePressed(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseReleased(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseEntered(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseExited(MouseEvent mouseEvent) {

    }
}
