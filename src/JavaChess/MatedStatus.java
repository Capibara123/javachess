package JavaChess;

public enum MatedStatus {
    WhiteMated, BlackMated, StaleMate,NoOneMated
}
