package JavaChess.Pieces;

import JavaChess.Tile;

import java.util.ArrayList;
import java.util.List;

public class Pawn extends Piece {

    private boolean moved;

    public Pawn(int x, int y, Colour c) {
        super(x, y, c);
        value = 1;
    }

    @Override
    public void Move(Tile[][] board, Tile to) {
        if(checkIfValidMove(board, to)) {
            board[to.y][to.x].piece = null;
            board[to.y][to.x].piece = this;
            board[y][x].piece = null;
            x = to.x;
            y = to.y;
            if(!moved) {
                moved = true;
            }

            // Promotion
            if(y == 0 && colour == Colour.White) {
                board[y][x].piece = new Queen(x,y, Colour.White);
            }
        }
    }

    @Override
    public String toString() {
        return "P";
    }

    @Override
    public Tile[] getMoves(Tile[][] board) {
        // TÄSTÄ EI SIT PUHUTA, varmaa enemmän if lauseita ku yandevin koodis
        List<Tile> validTiles = new ArrayList<Tile>();
        Tile ownTile = board[y][x];
        if(!moved) {
            if(colour == Colour.White) {
                if(board[y-2][x].piece == null && board[y-1][x].piece == null) {
                    validTiles.add(board[y-2][x]);
                }
            }
            else {
                if(board[y+2][x].piece == null && board[y+1][x].piece == null) {
                    validTiles.add(board[y+2][x]);
                }
            }
        }
        if(colour == Colour.White) {
            if(y-1 >= 0) {
                if(board[y-1][x].piece == null) {
                    validTiles.add(board[y-1][x]);
                }
                if(x-1 >= 0) {
                    if (board[y - 1][x - 1].piece != null ) {
                        if(board[y-1][x-1].piece.colour != colour) {
                            validTiles.add(board[y - 1][x - 1]);
                        }
                    }
                }
                if(x+1 <= 7) {
                    if (board[y - 1][x + 1].piece != null) {
                        if(board[y-1][x+1].piece.colour != colour){
                            validTiles.add(board[y - 1][x + 1]);
                        }
                    }
                }
            }
        }
        else {
            if(y+1 <= 7) {
                if(board[y+1][x].piece == null) {
                    validTiles.add(board[y+1][x]);
                }
                if(x+1 <= 7) {
                    if(board[y+1][x+1].piece != null) {
                        if(board[y+1][x+1].piece.colour != colour) {
                            validTiles.add(board[y+1][x+1]);
                        }
                    }
                }
                if(x-1 >= 0) {
                    if(board[y+1][x-1].piece != null) {
                        if( board[y+1][x-1].piece.colour != colour) {
                            validTiles.add(board[y+1][x-1]);
                        }
                    }
                }
            }
        }
        return validTiles.toArray(new Tile[0]);
    }


}
