package JavaChess.Pieces;

import JavaChess.Tile;

import java.util.ArrayList;
import java.util.List;

public class Queen extends Piece {

    public Queen(int x, int y, Colour c) {
        super(x, y, c);
        value = 9;
    }


    @Override
    public String toString() {
        return "Q";
    }


    @Override
    public Tile[] getMoves(Tile[][] board) {
        List<Tile> validTiles = new ArrayList<Tile>();
        boolean completedDiagonal1 = false,completedDiagonal2  = false,completedDiagonal3 = false,completedDiagonal4 = false;
        boolean completedLine1 = false, completedLine2 = false,completedLine3 = false,completedLine4 = false;

        for (int i = 1; i < board.length; i++) {

            if(!completedDiagonal1) {
                try {
                    Tile tile1 = board[y + i][x + i];
                    completedDiagonal1 = checkTile(validTiles, tile1);
                } catch (IndexOutOfBoundsException e) {
                    completedDiagonal1 = true;
                }
            }


            if(!completedDiagonal2) {
                try {
                    Tile tile2 = board[y + i][x - i];
                    completedDiagonal2 = checkTile(validTiles, tile2);
                } catch (IndexOutOfBoundsException e) {
                    completedDiagonal2 = true;
                }
            }

            if(!completedDiagonal3) {
                try {
                    Tile tile3 = board[y - i][x + i];
                    completedDiagonal3 = checkTile(validTiles, tile3);
                } catch (IndexOutOfBoundsException e) {
                    completedDiagonal3 = true;
                }
            }

            if(!completedDiagonal4) {
                try {
                    Tile tile4 = board[y - i][x - i];
                    completedDiagonal4 = checkTile(validTiles, tile4);

                } catch (IndexOutOfBoundsException e) {
                    completedDiagonal4 = true;
                }
            }
            if(!completedLine1) {
                try {
                    Tile tile1 = board[y][x + i];
                    completedLine1 = checkTile(validTiles, tile1);
                } catch (IndexOutOfBoundsException e) {
                    completedLine1 = true;
                }
            }


            if(!completedLine2) {
                try {
                    Tile tile2 = board[y][x - i];
                    completedLine2 = checkTile(validTiles, tile2);
                } catch (IndexOutOfBoundsException e) {
                    completedLine2 = true;
                }
            }

            if(!completedLine3) {
                try {
                    Tile tile3 = board[y - i][x];
                    completedLine3 = checkTile(validTiles, tile3);
                } catch (IndexOutOfBoundsException e) {
                    completedLine3 = true;
                }
            }

            if(!completedLine4) {
                try {
                    Tile tile4 = board[y + i][x];
                    completedLine4 = checkTile(validTiles, tile4);

                } catch (IndexOutOfBoundsException e) {
                    completedLine4 = true;
                }
            }


            if(completedDiagonal1 && completedDiagonal2 && completedDiagonal3 && completedDiagonal4 && completedLine1 && completedLine2 && completedLine3 && completedLine4) { // JESUS CHRIST
                break;
            }
        }

        return validTiles.toArray(new Tile[0]);
    }

    private boolean checkTile(List<Tile> validTiles, Tile tile) {
        boolean checktile = false;
        if(tile.piece != null) {
            if(tile.piece.colour == colour) {
                checktile = true;
            } else {
                validTiles.add(tile);
                checktile = true;
            }
        } else {
            validTiles.add(tile);
        }
        return checktile;
    }
}
