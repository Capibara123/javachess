package JavaChess.Pieces;

import JavaChess.Tile;

import java.util.ArrayList;
import java.util.List;

public class Knight extends Piece {
    public Knight(int x, int y, Colour c) {
        super(x, y, c);
        value = 3;
    }

    @Override
    public void Move(Tile[][] board, Tile to) {
        if(checkIfValidMove(board, to)) {
            board[to.y][to.x].piece = null;
            board[to.y][to.x].piece = this;
            board[y][x].piece = null;
            x = to.x;
            y = to.y;
        }
    }

    @Override
    public String toString() {
        return "N";
    }

    @Override
    public Tile[] getMoves(Tile[][] board) {
        List<Tile> validTiles = new ArrayList<Tile>();
        try {
            Tile tile1 = board[y+2][x+1];
            checkTile(validTiles, tile1);
        } catch (IndexOutOfBoundsException ignored) {
        }
        try {
            Tile tile2 = board[y-2][x+1];
            checkTile(validTiles, tile2);
        } catch (IndexOutOfBoundsException ignored) {
        }

        try {
            Tile tile3 = board[y-2][x-1];
            checkTile(validTiles, tile3);
        }catch (IndexOutOfBoundsException ignored) {
        }

        try {
            Tile tile4 = board[y+2][x-1];
            checkTile(validTiles, tile4);
        } catch (IndexOutOfBoundsException ignored) {
        }

        try {
            Tile tile5 = board[y+1][x+2];
            checkTile(validTiles, tile5);
        } catch (IndexOutOfBoundsException ignored) {
        }
        try {
            Tile tile6 = board[y-1][x+2];
            checkTile(validTiles, tile6);
        } catch (IndexOutOfBoundsException ignored) {
        }
        try {
            Tile tile7 = board[y-1][x-2];
            checkTile(validTiles, tile7);
        } catch (IndexOutOfBoundsException ignored) {
        }
        try {
            Tile tile8 = board[y+1][x-2];
            checkTile(validTiles, tile8);
        } catch (IndexOutOfBoundsException ignored) {
        }
        return validTiles.toArray(new Tile[0]);
    }

    private void checkTile(List<Tile> validTiles, Tile tile) {
        if(tile.piece != null) {
            if(tile.piece.colour != colour) {
                validTiles.add(tile);
            }
        } else {
            validTiles.add(tile);
        }
    }

}
