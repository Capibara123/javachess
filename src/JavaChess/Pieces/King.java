package JavaChess.Pieces;

import JavaChess.Tile;

import java.util.ArrayList;
import java.util.List;

public class King extends Piece {
    public boolean inCheck = false;
    boolean moved = false;

    public King(int x, int y, Colour c) {
        super(x, y, c);
        value = 30;
    }

    public boolean isInCheck(Tile[][] board) {
        for (Tile[] row: board) {
            for (Tile tile: row) {
                if(tile.piece == null) {
                    continue;
                }
                if(tile.piece.colour == colour) {
                    continue;
                }
                Piece piece = tile.piece;
                Tile[] validMoves = piece.getMoves(board);
                for(Tile move: validMoves) {
                    if(move.piece == this) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    @Override
    public String toString() {
        return "K";
    }

    @Override
    public void Move(Tile[][] board, Tile to) {
        if(checkIfValidMove(board, to)) {
            if(board[to.y][to.x].piece instanceof Rook && board[to.y][to.x].piece.colour == colour) {
                switch (to.x) {
                    case 7:
                        board[to.y][to.x - 2].piece = board[to.y][to.x].piece;
                        board[to.y][to.x].piece = null;
                        board[to.y][to.x - 2].piece.x -= 2;

                        board[y][x + 2].piece = this;
                        board[y][x].piece = null;
                        x += 2;
                        break;
                    case 0:
                        board[to.y][to.x + 2].piece = board[to.y][to.x].piece;
                        board[to.y][to.x].piece = null;
                        board[to.y][to.x + 2].piece.x += 2;

                        board[y][x - 3].piece = this;
                        board[y][x].piece = null;
                        x -= 3;
                        break;
                }

            } else {
                board[to.y][to.x].piece = null;
                board[to.y][to.x].piece = this;
                board[y][x].piece = null;
                x = to.x;
                y = to.y;
            }

        }
        if(!moved) {
            moved = true;
        }
    };



    @Override
    public Tile[] getMoves(Tile[][] board) {
        List<Tile> validTiles = new ArrayList<Tile>();
        try {
            Tile tile1 = board[y+1][x+1];
            checkTile(validTiles, tile1, board);
        } catch (IndexOutOfBoundsException ignored) {
        }
        try {
            Tile tile2 = board[y-1][x+1];
            checkTile(validTiles, tile2, board);
        } catch (IndexOutOfBoundsException ignored) {
        }

        try {
            Tile tile3 = board[y-1][x-1];
            checkTile(validTiles, tile3, board);
        }catch (IndexOutOfBoundsException ignored) {
        }

        try {
            Tile tile4 = board[y+1][x-1];
            checkTile(validTiles, tile4, board);
        } catch (IndexOutOfBoundsException ignored) {
        }

        try {
            Tile tile5 = board[y+1][x];
            checkTile(validTiles, tile5, board);
        } catch (IndexOutOfBoundsException ignored) {
        }
        try {
            Tile tile6 = board[y-1][x];
            checkTile(validTiles, tile6, board);
        } catch (IndexOutOfBoundsException ignored) {
        }
        try {
            Tile tile7 = board[y][x+1];
            checkTile(validTiles, tile7, board);
        } catch (IndexOutOfBoundsException ignored) {
        }
        try {
            Tile tile8 = board[y][x-1];
            checkTile(validTiles, tile8, board);
        } catch (IndexOutOfBoundsException ignored) {
        }

        // Check if can castle
        if(!inCheck && !moved) {
            // Check for short castle
            boolean canCastleShort = true;
            if(board[y][7].piece instanceof Rook) {
                for (int i = 1; i < 3; i++) {
                    Tile TileToCheck = board[y][x+i];
                    if(TileToCheck.piece != null) {
                        canCastleShort = false;
                        break;
                    }
                }
            }
            if(canCastleShort) {
                validTiles.add(board[y][7]);
            }

            // Check for long castle
            boolean canCastleLong = true;
            if(board[y][0].piece instanceof Rook) {
                for (int i = 1; i < 3; i++) {
                    Tile TileToCheck = board[y][x-i];
                    if(TileToCheck.piece != null) {
                        canCastleLong = false;
                        break;
                    }
                }
            }
            if(canCastleLong) {
                validTiles.add(board[y][0]);
            }

        }
        return validTiles.toArray(new Tile[0]);
    }

    private void checkTile(List<Tile> validTiles, Tile tile, Tile[][] board) {
        if(tile.piece != null) {
            if(tile.piece.colour != colour) {
                validTiles.add(tile);
            }
        } else {
            validTiles.add(tile);
        }
    }


}
