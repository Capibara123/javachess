package JavaChess.Pieces;

import JavaChess.Tile;

import java.util.ArrayList;
import java.util.List;

public class Rook extends Piece {
    public Rook(int x, int y, Colour c) {
        super(x, y, c);
        value = 5;
    }


    @Override
    public String toString() {
        return "R";
    }


    @Override
    public Tile[] getMoves(Tile[][] board) {
        List<Tile> validTiles = new ArrayList<Tile>();
        boolean completedLine1 = false, completedLine2 = false,completedLine3 = false,completedLine4 = false;
        for (int i = 1; i < board.length; i++) {

            if(!completedLine1) {
                try {
                    Tile tile1 = board[y][x + i];
                    completedLine1 = checkLine(validTiles, tile1);
                } catch (IndexOutOfBoundsException e) {
                    completedLine1 = true;
                }
            }


            if(!completedLine2) {
                try {
                    Tile tile2 = board[y][x - i];
                    completedLine2 = checkLine(validTiles, tile2);
                } catch (IndexOutOfBoundsException e) {
                    completedLine2 = true;
                }
            }

            if(!completedLine3) {
                try {
                    Tile tile3 = board[y - i][x];
                    completedLine3 = checkLine(validTiles, tile3);
                } catch (IndexOutOfBoundsException e) {
                    completedLine3 = true;
                }
            }

            if(!completedLine4) {
                try {
                    Tile tile4 = board[y + i][x];
                    completedLine4 = checkLine(validTiles, tile4);

                } catch (IndexOutOfBoundsException e) {
                    completedLine4 = true;
                }
            }


            if(completedLine1 && completedLine2 && completedLine3 && completedLine4) {
                break;
            }
        }

        return validTiles.toArray(new Tile[0]);
    }

    private boolean checkLine(List<Tile> validTiles, Tile tile) {
        boolean completedLine = false;
        if(tile.piece != null) {
            if(tile.piece.colour == colour) {
                completedLine = true;
            } else {
                validTiles.add(tile);
                completedLine = true;
            }
        } else {
            validTiles.add(tile);
        }
        return completedLine;
    }
}
