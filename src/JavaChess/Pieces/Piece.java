package JavaChess.Pieces;

import JavaChess.MatedStatus;
import JavaChess.Tile;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;


public abstract class Piece {
    public int value;
    private BufferedImage image;
    public Colour colour;
    public int x,y;


    Piece(int x, int y, Colour c) {
        colour = c;
        this.x = x;
        this.y = y;
        String imgPath = getImagePath();
        try {
            image = ImageIO.read(new File(imgPath));
        } catch (IOException e) {
            System.out.println("File not found");
            image = null;
        }
    }


    public void Move(Tile[][] board, Tile to) {
        if(checkIfValidMove(board, to)) {
            board[to.y][to.x].piece = null;
            board[to.y][to.x].piece = this;
            board[y][x].piece = null;
            x = to.x;
            y = to.y;
        }
    };

    public abstract String toString();

    public boolean checkIfValidMove(Tile[][] board,Tile to) {
        Tile[] validMoves = this.getValidMoves(board);
        for (Tile t: validMoves) {
            if(to.x == t.x && to.y == t.y) {
                return true;
            }
        }
        return false;
    };

    public Object clone() throws CloneNotSupportedException {
        Object clone = super.clone();
        return clone;
    }

    public void setPosition(Tile[][] board, Tile to) {
        board[y][x].piece = null;
        board[to.y][to.x].piece = this;
        x = to.x;
        y = to.y;
    }

    public static King getKing(Tile[][] board, Colour c) {
        for (Tile[] row: board) {
            for (Tile tile: row) {
                if(tile.piece instanceof King && tile.piece.colour == c) {
                    return (King) tile.piece;
                }
            }
        }
        return null;
    }


    public Tile[] getValidMoves(Tile[][] board) {
        Tile[] allMoves = getMoves(board);
        List<Tile> validMoves = new ArrayList<Tile>();

        King ownKing = Piece.getKing(board, colour);
        assert ownKing != null;
        int curX = x;
        int curY = y;

        Tile curPos = board[curY][curX];

        for (Tile move: allMoves) {
            // Try the move and see if it puts the king in check
            if(move.piece != null) {
                if(move.piece.colour == colour) {
                    if (this instanceof King) {
                        switch (move.x) {
                            case 7:
                                Piece prevPiece = board[y][x+2].piece;
                                board[y][x + 2].piece = this;
                                board[y][x].piece = null;
                                x += 2;
                                if (((King) this).isInCheck(board)) {
                                    board[y][x - 2].piece = this;
                                    board[y][x].piece = prevPiece;
                                    x -= 2;
                                    break;
                                }
                                board[y][x - 2].piece = this;
                                board[y][x].piece = prevPiece;
                                x -= 2;
                                validMoves.add(move);
                                break;
                            case 0:
                                Piece previousPiece = board[y][x-3].piece;
                                board[y][x - 3].piece = this;
                                board[y][x].piece = null;
                                x -= 3;
                                if (((King) this).isInCheck(board)) {
                                    board[y][x + 3].piece = this;
                                    board[y][x].piece = previousPiece;
                                    x += 3;
                                    break;
                                }
                                board[y][x + 3].piece = this;
                                board[y][x].piece = previousPiece;
                                validMoves.add(move);
                                x += 3;
                                break;
                        }
                    }
                } else {
                    Piece posPreviousPiece = board[move.y][move.x].piece;
                    this.setPosition(board, move);

                    if(!ownKing.isInCheck(board)) {
                        validMoves.add(move);
                    }
                    this.setPosition(board, curPos);
                    posPreviousPiece.setPosition(board, board[move.y][move.x]);
                }
            } else {
                this.setPosition(board, move);

                if(!ownKing.isInCheck(board)) {
                    validMoves.add(move);
                }
                this.setPosition(board, curPos);
            }
        }

        return validMoves.toArray(new Tile[0]);
    };

    public abstract Tile[] getMoves(Tile[][] board);

    public int getValue() {
        if(colour == Colour.White) {
            return value;
        } else {
            return -value;
        }
    }

    public static MatedStatus checkForMate(Tile[][] board) {
        // Count the pieces:
        int pieceCount = 0;
        for (Tile[] row:board) {
            for (Tile tile : row) {
                if (tile.piece != null) {
                    pieceCount++;
                }
            }
        }
        if(pieceCount == 2) {
            return MatedStatus.StaleMate;
        }

        boolean whiteCanMove = false;
        boolean blackCanMove = false;

        for (Tile[] row:board) {
            for(Tile tile : row) {
                if(tile.piece != null) {
                    if(tile.piece.colour == Colour.White) {
                        if(tile.piece.getValidMoves(board).length > 0) {
                            whiteCanMove = true;
                            break;
                        }
                    }
                }
            }
        }

        for (Tile[] row:board) {
            for(Tile tile : row) {
                if(tile.piece != null) {
                    if(tile.piece.colour == Colour.Black) {
                        if(tile.piece.getValidMoves(board).length > 0) {
                            blackCanMove = true;
                            break;
                        }
                    }
                }
            }
        }

        King blackKing = getKing(board, Colour.Black);
        King whiteKing = getKing(board, Colour.White);

        assert whiteKing != null;
        if(whiteKing.isInCheck(board) && !whiteCanMove) {
            return MatedStatus.WhiteMated;
        } else {
            assert blackKing != null;
            if (blackKing.isInCheck(board) && !blackCanMove) {
                return MatedStatus.BlackMated;
            } else if (!whiteCanMove || !blackCanMove) {
                return MatedStatus.StaleMate;
            } else {
                return MatedStatus.NoOneMated;
            }
        }

    }


    private String getImagePath() {
        Path currentRelativePath = Paths.get("");
        String path = currentRelativePath.toAbsolutePath().toString();
        String letter = toString().toLowerCase();
        String color = colour == Colour.White? "l" : "d";
        StringBuilder sb = new StringBuilder();
        sb.append(path);
        sb.append("\\images\\Chess_");
        sb.append(letter);
        sb.append(color);
        sb.append("t60.png");
        return sb.toString();
    }
    public Image getImage() {
        return image;
    }
}

