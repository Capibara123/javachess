package JavaChess.Pieces;

import JavaChess.Tile;

import java.util.ArrayList;
import java.util.List;

public class Bishop extends Piece {
    public Bishop(int x, int y, Colour c) {
        super(x, y, c);
        value = 3;
    }


    @Override
    public String toString() {
        return "B";
    }

    @Override
    public Tile[] getMoves(Tile[][] board) {
        List<Tile> validTiles = new ArrayList<Tile>();
        boolean completedDiagonal1 = false,completedDiagonal2  = false,completedDiagonal3 = false,completedDiagonal4 = false;
        for (int i = 1; i < board.length; i++) {

            if(!completedDiagonal1) {
                try {
                    Tile tile1 = board[y + i][x + i];
                    completedDiagonal1 = checkDiagonal(validTiles, tile1);
                } catch (IndexOutOfBoundsException e) {
                    completedDiagonal1 = true;
                }
            }


            if(!completedDiagonal2) {
                try {
                    Tile tile2 = board[y + i][x - i];
                    completedDiagonal2 = checkDiagonal(validTiles, tile2);
                } catch (IndexOutOfBoundsException e) {
                    completedDiagonal2 = true;
                }
            }

            if(!completedDiagonal3) {
                try {
                    Tile tile3 = board[y - i][x + i];
                    completedDiagonal3 = checkDiagonal(validTiles, tile3);
                } catch (IndexOutOfBoundsException e) {
                    completedDiagonal3 = true;
                }
            }

            if(!completedDiagonal4) {
                try {
                    Tile tile4 = board[y - i][x - i];
                    completedDiagonal4 = checkDiagonal(validTiles, tile4);

                } catch (IndexOutOfBoundsException e) {
                    completedDiagonal4 = true;
                }
            }


            if(completedDiagonal1 && completedDiagonal2 && completedDiagonal3 && completedDiagonal4) {
                break;
            }
        }

        return validTiles.toArray(new Tile[0]);
    }


    private boolean checkDiagonal(List<Tile> validTiles, Tile tile) {
        boolean completedDiagonal = false;
        if(tile.piece != null) {
            if(tile.piece.colour == colour) {
                completedDiagonal = true;
            } else {
                validTiles.add(tile);
                completedDiagonal = true;
            }
        } else {
            validTiles.add(tile);
        }
        return completedDiagonal;
    }
}
