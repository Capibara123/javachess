package JavaChess;

import JavaChess.Pieces.Piece;

public class Tile {
    public int x;
    public int y;
    public Piece piece;

    public boolean isIn(Tile[] tiles) {
        if(tiles==null) {
            return false;
        }
        for (Tile t: tiles) {
            if(t.x == x && t.y == y) {
                return true;
            }
        }
        return false;
    }

    public int getValue() {
        if(this.piece != null) {
            return this.piece.getValue();
        } else {
            return 0;
        }
    }

}
